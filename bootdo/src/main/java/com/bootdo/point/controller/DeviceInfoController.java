package com.bootdo.point.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;
import com.bootdo.point.domain.DeviceInfoDO;
import com.bootdo.point.domain.RemoteDO;
import com.bootdo.point.service.DeviceInfoService;
import com.point.mina.client.AdminClient;

/**
 * 设备管理
 * 
 * @author langxianwei
 * @email 1992lcg@163.com
 * @date 2018-11-25 18:39:37
 */
 
@Controller
@RequestMapping("/point/deviceInfo")
public class DeviceInfoController {
	@Autowired
	private DeviceInfoService deviceInfoService;
	
	@GetMapping()
	@RequiresPermissions("point:deviceInfo:deviceInfo")
	String DeviceInfo(){
	    return "point/deviceInfo/deviceInfo";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("point:deviceInfo:deviceInfo")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<DeviceInfoDO> deviceInfoList = deviceInfoService.list(query);
		int total = deviceInfoService.count(query);
		PageUtils pageUtils = new PageUtils(deviceInfoList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("point:deviceInfo:add")
	String add(){
	    return "point/deviceInfo/add";
	}

	@GetMapping("/edit/{deviceId}")
	@RequiresPermissions("point:deviceInfo:edit")
	String edit(@PathVariable("deviceId") String deviceId,Model model){
		DeviceInfoDO deviceInfo = deviceInfoService.get(deviceId);
		model.addAttribute("deviceInfo", deviceInfo);
	    return "point/deviceInfo/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("point:deviceInfo:add")
	public R save( DeviceInfoDO deviceInfo){
		if(deviceInfoService.save(deviceInfo)>0){
			return R.ok();
		}
		return R.error();
	}
	@GetMapping("/toRemoteTemp/{deviceId}")
	@RequiresPermissions("point:deviceInfo:remoteTemperature")
	String toRemoteTemp(@PathVariable("deviceId") String deviceId,Model model){
		DeviceInfoDO deviceInfo = deviceInfoService.get(deviceId);
		model.addAttribute("deviceInfo", deviceInfo);
	    return "point/deviceInfo/remoteTemperature";
	}	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/remoteTemp")
	@RequiresPermissions("point:deviceInfo:remoteTemperature")
	public R remoteTemp( RemoteDO remoteDO){
		AdminClient.getInstance().sendTemperatureData(remoteDO.getDeviceId(), remoteDO.getSessionId(), remoteDO.getMaxTemp(), remoteDO.getMinTemp());
		return R.ok();
	}	
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("point:deviceInfo:edit")
	public R update( DeviceInfoDO deviceInfo){
		deviceInfoService.update(deviceInfo);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("point:deviceInfo:remove")
	public R remove( String deviceId){
		if(deviceInfoService.remove(deviceId)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("point:deviceInfo:batchRemove")
	public R remove(@RequestParam("ids[]") String[] deviceIds){
		deviceInfoService.batchRemove(deviceIds);
		return R.ok();
	}
	
}
