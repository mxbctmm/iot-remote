package com.point.mina.client.entitiy;

import com.point.iot.base.message.PointMessage;

public class RemoteControlRequestBean extends PointMessage{
	/**
	 * 设备ID
	 */
	private int deviceId;
	/**
	 * 高温报警溫度
	 */
	private short maxTemperature;
	/**
	 * 低温报警溫度
	 */
	private short minTemperature;
	
	public int getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}
	public short getMaxTemperature() {
		return maxTemperature;
	}
	public void setMaxTemperature(short maxTemperature) {
		this.maxTemperature = maxTemperature;
	}
	public short getMinTemperature() {
		return minTemperature;
	}
	public void setMinTemperature(short minTemperature) {
		this.minTemperature = minTemperature;
	}
	
}
